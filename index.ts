import { NgModule, ModuleWithProviders }              from '@angular/core';
import { CommonModule }                               from '@angular/common';
import { NgRedux, NgReduxModule, DevToolsExtension }  from "@angular-redux/store";
import { FormsModule }                                from "@angular/forms";
import { NgReduxRouter }                              from "@angular-redux/router";

import { ApplicationRoutingModule }                   from "./src/application-routing.module";
import { ApplicationComponent }                       from "./src/application.component";

import {
    AuthGuardService,
    HttpService,
    WebApiService,
    WebApiUrlService,
    IAppState
}                           from "@microbizllc/application-shared";

import { rootReducer, enhancers }     from "./src/store";
import { ErrorActions }               from "./src/store/error/error.actions";

import { Http, HttpModule } from "@angular/http";
import { MbizSharedModule } from '@microbizllc/shared';
import { MbizApplicationLoginModule }  from '@microbizllc/application-login';
import { MbizApplicationBrandModule }  from '@microbizllc/application-brand';

export * from './src/store';
export * from './src/application.component';

@NgModule( {
    imports:         [
        CommonModule,
        FormsModule,
        HttpModule,
        NgReduxModule,
        ApplicationRoutingModule,

        MbizSharedModule,
        MbizApplicationLoginModule,
        MbizApplicationBrandModule,
    ],
    declarations:    [
        ApplicationComponent,
    ],
    entryComponents: [ ApplicationComponent ],
    bootstrap:       [ ApplicationComponent ],
    providers:       [
        NgReduxRouter,
        ErrorActions,
    ],
    exports:         [ ApplicationComponent ]
} )
export class MbizApplicationModule {

    constructor( private ngRedux: NgRedux<IAppState>,
                 private ngReduxRouter: NgReduxRouter,
                 private devTool: DevToolsExtension ) {

        this.configureStore( devTool );
        this.initializeRouter()
    }

    private configureStore( devTool: DevToolsExtension ) {

        this.ngRedux.configureStore(
            rootReducer,
            {},
            [],
            [ ...enhancers, devTool.isEnabled() ? devTool.enhancer() : f => f ]
        );

    }

    private initializeRouter() {

        this.ngReduxRouter.initialize();

    }

    static forRoot(): ModuleWithProviders {
        return {
            ngModule:  MbizApplicationModule,
            providers: [

                NgReduxRouter,
                ErrorActions,

            ]
        };
    }
}
