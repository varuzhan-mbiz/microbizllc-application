import { combineReducers }  from 'redux';
import { routerReducer }    from "@angular-redux/router";

import { IAppState } from '@microbizllc/application-shared';

import { errorReducer }     from "./error/error.reducer";
import { loginReducer }     from "@microbizllc/application-login";
import { brandReducer }     from "@microbizllc/application-brand";

//const persistState = require( 'redux-localstorage' );

export const rootReducer = combineReducers<IAppState>( {
    brands: brandReducer,
    login:  loginReducer,
    error:  errorReducer,
    router: routerReducer
} );

export const enhancers = [
    //persistState( 'instances', { key: 'application' } )
];