import { Injectable } from "@angular/core";
import { NgRedux }    from "@angular-redux/store";
import { IAppState }  from "@microbizllc/application-shared";

@Injectable()
export class ErrorActions {

    constructor( private ngRedux:NgRedux<IAppState>){  }

    static ERROR_OCCURRED = 'ERROR_OCCURRED';

    public errorOccurred( data?:any ){

        this.ngRedux.dispatch( { type: ErrorActions.ERROR_OCCURRED, data: data } );

    }

}