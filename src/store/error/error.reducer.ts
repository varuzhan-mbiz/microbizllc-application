import { ErrorActions } from "./error.actions";

let INITIAL_STATE = {
    data: <any>null,
};

function assign_( ...args:any[] ){ return Object.assign( {}, ...args ); }

export function errorReducer( state = INITIAL_STATE, action:any ) {

    switch ( action.type ) {

        case ErrorActions.ERROR_OCCURRED : {

            let currentState = { data: action.data };

            return assign_( state, currentState );

        }

        default: return state;

    }

}