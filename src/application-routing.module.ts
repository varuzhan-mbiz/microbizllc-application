import { NgModule }                  from "@angular/core";
import { RouterModule }              from "@angular/router";

import { AuthGuardService }          from "@microbizllc/application-shared";
import { ApplicationBrandComponent } from "@microbizllc/application-brand";
import { ApplicationLoginComponent } from "@microbizllc/application-login";

import { ApplicationComponent } from "./application.component";

export const authProviders = [ AuthGuardService ];

@NgModule({
    imports:[
        RouterModule.forChild([
            { path: "", component: ApplicationComponent, children: [
                { path: "login", component: ApplicationLoginComponent, canActivate: authProviders },
                { path: "brand", component: ApplicationBrandComponent, canActivate: authProviders },
            ] },
        ])
    ],
    exports:[ RouterModule ]
})

export class ApplicationRoutingModule {}