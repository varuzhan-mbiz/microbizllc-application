import { Component, OnInit } from '@angular/core';
import { LoginActions } from "@microbizllc/application-login";
import { select } from "@angular-redux/store";

@Component( {
    selector:    'mbiz-application',
    templateUrl: './application.component.html',
    styleUrls:   [ './application.component.scss' ]
} )
export class ApplicationComponent implements OnInit {

    @select() login$:any;
    
    constructor(
        public login_actions: LoginActions
    ) {
    }

    ngOnInit() {
    }

}
